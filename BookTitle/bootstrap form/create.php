<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap.css"type="text/css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>

    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="jumbotron ">
    <h2 align="center">heading</h2>
    </div>
<div class="container">
    <div class="page-header">
        </div>

    <div class="row">
    <div class="col-lg-offset-3 col-sm-offset-3 col-md-offset-3 col-xs-offset-3 col-lg-3 col-sm-3 col-md-3 col-xs-3">
     <marquee style="color: #1b6d85">please fill the form</marquee>
    <h4 style="color: #2aabd2" >Vertical (basic) form</h4>
    </div>
    </div>
    <div class="row">
        <div class="col-lg-offset-3 col-sm-offset-3 col-md-offset-3 col-xs-offset-3 col-lg-3 col-sm-3 col-md-3 col-xs-3">
    <form class="well"style="background-color: #2aabd2">

        <div class="form-group" >
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" placeholder="Enter password">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
    </form>
</div>

</div>

</body>
</html>

